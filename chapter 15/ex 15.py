#!/usr/bin/env python3

#gets argv module from sys
from sys import argv

#unpackes terminal parameters to script ans filename
script, filename = argv

#open the file filename and assigns it to txt
txt = open(filename)

#prints msg to screen
print(f"Here's your file {filename}:")
#prints txt file content to screen via txt's read method
print(txt.read())

# #prints msg to screen
# print("Type the file name again:")
# #prompts user for the new filename
# file_again = input("> ")

# #open the new filename
# txt_again = open(file_again)
# #prints new filename's content to screen
# print(txt_again.read())

