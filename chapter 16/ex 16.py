#!/usr/bin/env python3

from sys import argv

script, filename = argv

print(f"Wer'e going to erase {filename}.")
print("If you don't want that, hit ctrl-C (^C).")
print("Tf you do want that, hit RETURN.")

input("?")

print("Opening the file...")
target = open(filename, 'w')

print("Now I'm going to ask you for three lines:")

content = input("line 1: ")
content = content + "\n" + input("line 2: ")
content = content + "\n" + input("line 3: ")

print("I'm going to write these to the file.")

target.write(content)

print("And finally, we close it.")
target.close()