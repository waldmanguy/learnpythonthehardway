#!/usr/bin/env python3

#create 4 place holders as a string in a variable
formatter = "{} {} {} {}"
#print the string with numbers in the place holders
print(formatter.format(1,2,3,4))
#print the string with mini strings in these placeholders
print(formatter.format("one","two","three","four"))
#print the string with booleans in these placeholders
print(formatter.format(True,False,False,True))
#print the string with himself in these placeholders
print(formatter.format(formatter,formatter,formatter,formatter))
#print the string with mini strings in these placeholders- indented
print(formatter.format(
    "Try your",
    "Own text here",
    "Maybe a poem",
    "Or a song abot fear"
))