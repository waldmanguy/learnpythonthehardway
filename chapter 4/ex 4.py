#!/usr/bin/env python3

#number of cars
cars = 100
#number of places in a car
space_in_a_car = 4.0
#number of poeple driving a car
drivers = 30
#number of people not driving a car
passengers = 90
#number of cars not driven
car_not_driven = cars - drivers
#number of car driven
cars_driven = drivers
#number of people the carpool is able to accomodate
carpool_capacity = cars_driven * space_in_a_car
#average number of people in a car
average_passengers_per_car = passengers / cars_driven

print("There are", cars, "cars available")
print("There are only", drivers, "drivers available")
print("There will be", car_not_driven, "empty cars today.")
print("We can transport", carpool_capacity, "people today.")
print("We have", passengers, "to carpool today.")
print("We need to put about", average_passengers_per_car, "in each car.")

