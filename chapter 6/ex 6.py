#!/usr/bin/env python3

#number of types of people (in binary haha)
types_of_people = 10
#the formatted string of a statement in a variable
x = f"There are {types_of_people} types of people."

binary = "binary"
do_not = "don't"
#another formatted statement in a variable
y = f"Those who know {binary} and those who {do_not}."

#printing the statements
print(x)
print(y)
#formatted printing the statements
print(f"I said: {x}")
print(f"I also said: {y}")

hilarious = False
joke_evaluation = "Isn't that joke so funny?! {}"

print(joke_evaluation.format(hilarious))
#concatinating two strings
w = "This is the left side of..."
e = "a string with a right side."

print(w + e)