#!/usr/bin/env python3
#output string to screen
print("I will now count my chickens:")
#counting hens
print("Hens", 25 + 30 / 6)
#counting roosters
print("Roosters", 100.0 - 25 * 3 % 4)
#output a string to screen
print("Now I will count the eggs:")
#counting the eggs
print(3.0+2+1-5+4%2-1/4+6)
#asking if the statement is true
print("Is it true that 3 + 2 < 5 - 7?")
#printing boolean answer
print( 3 + 2 < 5 - 7)
#checking left statement
print("What is 3 + 2?", 3 + 2)
#checking right statement
print("What is 5 - 7?", 5 - 7)
#outputting to screen
print("Oh, that's why it's False.")
#outputting to screen
print("How abount some more.")
#checking greater sign
print("Is it greater?", 5 > -2 )
#checking greater than or equal sign
print("Is it greater or equal?", 5 >= -2)
#checking lesser than or equal sign
print("Is it less or equal?", 5 <= -2)