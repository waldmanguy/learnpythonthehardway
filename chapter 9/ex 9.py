#! /usr/bin/env python3

#Here's some strange new stuff, remember type it exactly

#a string representing weekdays is saved to a variable
days = "Mon Tue Wed Thu Fri Sat Sun"
#a string representing months delimitd by \n is saved to a variable
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

#printing weekdays
print("Here are the days: ",days)
#printing monthes
print("Here are the months: ", months)

#printing a multilined string with """
print("""
There's something going on here.
With the three double-quotes.
We'll be able to type as much as we like.
Even 4 line if we want, or 5, or 6.
      """)