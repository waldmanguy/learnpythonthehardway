#!/usr/bin/env python3

name = 'Zed A. Shaw'
age = 35
height = 74 #inches

inches_to_centimeters = 2.6

weight = 180 #pounds

pounds_to_kilograms = 0.5

eyes = 'Blue'
teeth = 'White'
hair = 'Brown'

print(f"Let's toalk about {name}")
print(f"He's {height} inches tall.")
print(f"Which are {height * inches_to_centimeters} centimeters tall.")
print(f"He's {weight} pounds heavy.")
print(f"Which are {weight * pounds_to_kilograms} kilograms heavy.")
print(f"Actually that's not too heavy.")
print(f"He's got {eyes} eyes and {hair} hair")
print(f"His teeth are usually {teeth} depending on the cofee.")

total = age + height + weight
print(f"If I add {age}, {height}, and {weight} I get {total}.")
